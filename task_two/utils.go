package main

import "fmt"

func getIndexOfStringElement(element string, slice []string) int {
	for i, str := range slice {
		if str == element {
			return i
		}
	}
	return -1
}

func getIndicesForStrings(elements []string, fromSlice []string) []int {
	var indices []int

	for _, element := range elements {
		i := getIndexOfStringElement(element, fromSlice)
		if i == -1 { // assume it never returns -1. this is for debugging.
			fmt.Println("not found lala")
		}
		indices = append(indices, i)
	}
	return indices
}

func getStringsAtIndices(indices []int, slice []string) []string {
	var strings []string

	for _, index := range indices {

		strings = append(strings, slice[index])
	}
	return strings

}

func getRowIndexWhereIthIsColumnEqualTo(matrix [][]string, columnIndex int, value string) int {
	for i, row := range matrix {
		if row[columnIndex] == value {
			return i
		}
	}
	return -1
}
func getRowIndexWhereTwoColumnsEqualTo(matrix [][]string, columnIndices []int, values []string) int {
	for i, row := range matrix {
		if (row[columnIndices[0]] == values[0]) && (row[columnIndices[1]] == values[1]) {
			return i
		}
	}
	return -1
}

func containsStr(slice []string, element string) bool {
	for _, el := range slice {
		if el == element {
			return true
		}
	}
	return false
}
