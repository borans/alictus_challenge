package main

//I haven't done individual parsers for each supplier to standardize the values because it is too much trivial work as you know.
//My code puts everything in place, in their original format.
//Basically, we can have a global dictionary which maps suppliers to formatter functions that format strings individually.
//Similar to how we map suppliers to ordered column names with "orderedSupplierColumnNames" dictionary.
import (
	"encoding/csv"
	"fmt"
	"os"
)

//these are for an ugly trick for putting NOTFOUND
var twoNotFound = []string{"NOTFOUND", "NOTFOUND"}
var threeNotFound = []string{"NOTFOUND", "NOTFOUND", "NOTFOUND"}

var orderedSupplierColumnNames = map[string][]string{
	"A": []string{"app_id", "Company_ID", "Item_Unit_Code",
		"DATE", "COUNTRY_CODE", "Item_Unit_format", "Revenue", "Item_count"},

	"B": []string{"app_id", "item_unit_id", "app_id", "Day",
		"Country", "Size", "Revenue", "no_items"},

	"C": []string{"app_id", "app_id", "itemunit_id", "time",
		"country", "display_format", "value", "value"},

	"D": []string{"source_game_id", "platform", "source_game_id", "timestamp",
		"country", "ItemUnit", "item_count", "revenue_sum"}}

func main() {
	//TODO: split blocks into functions
	//TODO: comment things

	finalFile, _ := os.Create("final.csv")
	unitHashFile, _ := os.Open("../DataSets/dataset/hashes/supplier_manager/unitsHash.csv")
	managerFile, _ := os.Open("../DataSets/dataset/raw/supplier_E/supplier_E_2019_12_01.csv")

	//read unit hash file
	reader := csv.NewReader(unitHashFile)
	reader.Read() // get rid of header line
	unitHashRows, _ := reader.ReadAll()
	defer unitHashFile.Close()
	unitHashColumnsToCompare := []int{3, 4} // indices of Supplier_Data_1 and 2.

	//read manager file
	reader = csv.NewReader(managerFile)
	reader.Read() //get rid of the header line
	managerRows, _ := reader.ReadAll()
	defer managerFile.Close()

	//init and flush writer
	writer := csv.NewWriter(finalFile)
	defer writer.Flush()

	//write file header
	writer.Write([]string{"Date", "App", "Platform", "Country", "Item_Unit_ID",
		"Item_Unit_name", "Item_Unit_format", "Item_Unit_Supplier", "Item_Unit_count",
		"Item_Unit_revenue", "Item_Unit_SupE_count"})

	for _, supplierLetter := range []string{"A", "B", "C", "D"} {
		fmt.Println(fmt.Sprintf("Starting with Supplier %s", supplierLetter))

		thisSupplierOrderedColumnNames := orderedSupplierColumnNames[supplierLetter]

		supplierFile, _ := os.Open(fmt.Sprintf("../DataSets/dataset/raw/supplier_%s/supplier_%s_2019_12_01.csv", supplierLetter, supplierLetter))
		keyFile, _ := os.Open(fmt.Sprintf("../DataSets/dataset/hashes/key_tags/Supplier_%s_keys.csv", supplierLetter))

		reader = csv.NewReader(supplierFile)
		fileColumnNames, _ := reader.Read()
		supplierIndices := getIndicesForStrings(thisSupplierOrderedColumnNames, fileColumnNames)
		supplierRows, _ := reader.ReadAll()
		defer supplierFile.Close()

		reader = csv.NewReader(keyFile)
		keyRows, _ := reader.ReadAll()
		defer keyFile.Close()
		keyRowLength := len(keyRows[0])

		numberOfRowsWithMissingDataThisBatch := 0
		numberOfRowsWithMissingDataTotal := 0

		for e, supplierRow := range supplierRows {
			//TODO: comment
			supplierRowVars := getStringsAtIndices(supplierIndices, supplierRow)

			keyRowIndex := getRowIndexWhereIthIsColumnEqualTo(keyRows, keyRowLength-1, supplierRowVars[0])

			keyRowVars := twoNotFound
			if keyRowIndex != -1 {
				keyRow := keyRows[keyRowIndex]
				keyRowVars = keyRow[:2]
			}

			unitHashRowVars := threeNotFound
			managerNoItems := "NOTFOUND"

			unitHashRowIndex := getRowIndexWhereTwoColumnsEqualTo(unitHashRows, unitHashColumnsToCompare, supplierRowVars[1:3])
			if unitHashRowIndex != -1 {
				unitHashRow := unitHashRows[unitHashRowIndex]
				unitHashRowVars = unitHashRow[:3]

				managerRowIndex := getRowIndexWhereIthIsColumnEqualTo(managerRows, 8, unitHashRowVars[1])
				managerRow := managerRows[managerRowIndex]
				managerNoItems = managerRow[len(managerRow)-1]

			}

			finalRow := []string{supplierRowVars[3], keyRowVars[0], keyRowVars[1],
				supplierRowVars[4], unitHashRowVars[1], unitHashRowVars[0],
				supplierRowVars[5], unitHashRowVars[2], supplierRowVars[7],
				supplierRowVars[6], managerNoItems}

			writeErr := writer.Write(finalRow)

			if writeErr != nil { //for debugging.
				fmt.Println("lalala")
			}
			if containsStr(finalRow, "NOTFOUND") {
				numberOfRowsWithMissingDataThisBatch++
			}
			if e%4999 == 0 && e != 0 { //some info every 5k rows
				fmt.Println("Done with", e, "rows.")
				fmt.Println("Number of created rows that had missing data in the last 5000 rows:", numberOfRowsWithMissingDataThisBatch)
				numberOfRowsWithMissingDataTotal = numberOfRowsWithMissingDataTotal + numberOfRowsWithMissingDataThisBatch
				numberOfRowsWithMissingDataThisBatch = 0
				fmt.Println()
			}

		}
		defer writer.Flush()
		fmt.Println()
		fmt.Println("Done with this supplier")
		fmt.Println("total incomplete rows this file:", numberOfRowsWithMissingDataTotal+numberOfRowsWithMissingDataThisBatch)
		//fmt.Println(numberOfRowsWithMissingDataThisBatch)
	}

}
