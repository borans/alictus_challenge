import os

"""
The csv files provided had DOS line endings. I couldn't read them properly on my unix computer with golang. 
Somehow dos2unix didn't work either. I couln't find a simple solution in golang so I just wrote this.
This script just rewrites the files to new location so that line endings are fixed.
"""


dir_path= "../DataSets/dataset-2/raw/"
new_dir_path = "../DataSets/dataset-2-unix/"

if not os.path.exists(new_dir_path):
    os.makedirs(new_dir_path)

files = os.listdir(dir_path)

for file_name in files:
    full_path = dir_path+file_name
    file_name_no_extention = file_name.split(".")[0]
    lines = open(full_path).readlines()

    new_file_path = "".join([new_dir_path, file_name_no_extention, "_unix.csv"])
    with open(new_file_path, "w+") as f:
        for line in lines:
            f.write(line)