package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"runtime"
)

func filterOutHiddenFiles(fileInfos []os.FileInfo) []string {
	var filteredFileNames []string
	for _, file := range fileInfos {
		fn := file.Name()
		if fn[0] != 46 { //46 is dot.
			filteredFileNames = append(filteredFileNames, fn)
		}
	}
	return filteredFileNames
}

func fileReader(doneCh chan int, lineCh chan string, files []string, readDirectoryPath string) {
	for i, fileName := range files {

		if i == len(files)-1 { //last thread spawned
			doneCh <- 1 //close signal
		} else {
			doneCh <- -1
		}

		go func(fileName string) {
			file, _ := os.Open(readDirectoryPath + fileName)
			defer file.Close()

			scanner := bufio.NewScanner(file)
			scanner.Scan() // drop header

			for scanner.Scan() { //send lines to lineCh
				lineCh <- scanner.Text()
			}

			res := <-doneCh //the last  one to end will receive 1
			if res == 1 {   //am I the last one to end?
				close(lineCh) //then close the channel
			}
		}(fileName)
	}

}

func getHeaderLine(filePath string) string {
	file, _ := os.Open(filePath)
	reader := bufio.NewScanner(file)
	reader.Scan()
	header := reader.Text()
	file.Close()
	return header
}

func writeBatchToFile(filePath string, headerLine string, batch []string) {
	f, err := os.Create(filePath)
	if err != nil {
		fmt.Println("ooo") //for debugging. I didn't do any error handling.
	}

	writer := bufio.NewWriter(f)
	writer.WriteString(headerLine + "\n") //put header

	for _, line := range batch { //write lines
		writer.WriteString(line + "\n")
	}

	writer.Flush()
	defer f.Close()
}

func main() {

	//Input: (I didn't fix a CLI but you can change it here)
	k := 5
	baseName := "Base"
	//

	//I didn't use csv library because we do not need columnwise oparations. just merge the rows!
	doneChannel := make(chan int, runtime.NumCPU()) //capacity of doneChannel specifies max number of threads
	lineChannel := make(chan string)                // unbuffered channel. for transfering the lines to single

	writeDirectoryPath := "../DataSets/dataset-2/output/meged/"
	readDirectoryPath := "../DataSets/dataset-2-unix/"
	fileNameFormatter := "%s%s_part%d.csv"

	fmt.Println("Cleaning output directory")
	os.RemoveAll(writeDirectoryPath)
	os.Mkdir(writeDirectoryPath, 0700)

	fmt.Println("Listing input files")
	fileInfos, _ := ioutil.ReadDir(readDirectoryPath)

	files := filterOutHiddenFiles(fileInfos)
	headerLine := getHeaderLine(readDirectoryPath + files[0])

	fmt.Println("Starting readers.")
	go fileReader(doneChannel, lineChannel, files, readDirectoryPath)

	var thisBatch []string
	num := 0 //count until k before writing to a new file
	fileNumber := 1

	fmt.Println("Starting writer")
	for received := range lineChannel {
		//fmt.Println(received)
		thisBatch = append(thisBatch, received)
		num++
		if num == k {
			thisFilePath := fmt.Sprintf(fileNameFormatter, writeDirectoryPath, baseName, fileNumber)
			writeBatchToFile(thisFilePath, headerLine, thisBatch) // this function actually seems parallelizable.
			//I haven't tried, but we can spawn writer goroutines in the feature.
			thisBatch = make([]string, 0)
			num = 0
			fileNumber++
		}

	}
	if len(thisBatch) != 0 { // check if anything is left. Write remaining lines to new file
		thisFilePath := fmt.Sprintf(fileNameFormatter, writeDirectoryPath, baseName, fileNumber)
		writeBatchToFile(thisFilePath, headerLine, thisBatch)
	}

	fmt.Println("Done")

}
