package main

import "fmt"

//inspiration = C3 in this link->  https://udhos.github.io/golang-concurrency-tricks/
// this is a concept code that solves the concurrency part of the task.
//see actual_solution.go for the full solution of the task.
func reader(doneCh chan int, lineCh chan int) {

	for i := 0; i < 10; i++ { //iterate over files here.
		//u cant send more than the capacity of the channel. therefore when we try to send 1 or -1, it will block until one is poped in the routine below.
		if i == 9 { // if it is the last file
			doneCh <- 1 //send close signal
		} else {
			doneCh <- -1
		}

		go func() {
			for j := 1000000; j > 0; j-- { //send lines one by one over a loop
				lineCh <- j
			}

			res := <-doneCh //the last  one to end will receive 1 because that is the last one added to doneCh
			if res == 1 {   //am I the last one to end?
				close(lineCh)

			}
		}()
	}

	//fmt.Println(<-doneCh)

}

func notmain() { //changed name to "notmain" so that it is muted.
	doneChannel := make(chan int, 4) // put num_cpu here
	lineChannel := make(chan int, 5) //lines can go through buffered channel. because close signal will come last anyway.

	go reader(doneChannel, lineChannel)

	for received := range lineChannel {
		fmt.Println(received) //consume lines and do writing logic here.
	}

}
