# Notes
* Put the `DataSets` folder in the repo root. 

All my golang experience was from some school courses in concurrent, parallel and distributed programming.
And that was mostly using go routins, channels, waitgroups etc. to implement conceptual concurrency solutions. 

Therefore my code may be non-idiomatic. I am eager to learn more about it after this task actually :)

I am also putting the notes that I wrote. They are messy but have some information. 

## task two

I have commented about some issues about the data in the code. 

I haven't done individual string formatting, but I've built the code so that you can just add simple converter functions without much effort.
I explain it as comment in the solution file. 

Some values are in wrong column(for ex. revenue and count are flipped in supplier A's data.)

I didn't change them because this is just randomly generated data. 

I can explain more in a Skype call later. 

### To run task two: 

go to task_two folder(IDK how it works with relative paths in golang and didn't try. Just to be sure.)

then `go run solution.go utils.go`

output file will be put in task_two folder. output location wasn't specified in the task description. 

## task three

If you are using unix computer like me, you should run task_three/fix_line_endings.py before you run my solution. 

The issue with line endings is explained in that python script. 

ps. Actually you need to run it even if you use Windows, because my go code assumes input folder created by the python script anyway :)

otherwise you can change the paths in the data given. 

### To run task three:

go to task_three folder(for the same reason as task two)

`go run actual_solution.go`



soulution.go is just a concept solution that I wrote for the concurrency part of the task.
I built the actual solution based on that solution. 

